# docker_and_python_2022

---

[video 1](https://youtu.be/ikRaPCJp5lA) introduction to telegram-bot, intro to docker
 
[video 2](https://youtu.be/ExifjUv_vso) docker-compose, postgresql, ports, volumes, environment variables, network

[video 3](https://youtu.be/2eIiuOmFI2Y) postgresql, pgadmin 