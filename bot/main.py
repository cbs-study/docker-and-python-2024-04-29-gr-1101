import asyncio
import logging
import sys

from bot.src.echobot import  main

logging.basicConfig(level=logging.INFO, stream=sys.stdout)
asyncio.run(main())
