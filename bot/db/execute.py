from bot.db.connection import create_connection
from psycopg import OperationalError, connection


def execute_query(
        query: str
) -> None:
    try:
        with create_connection() as conn:
            conn.execute(query)
            conn.commit()
            print(f"Query executed successfully")
    except OperationalError as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    create_table_query = """
    CREATE TABLE IF NOT EXISTS message (
        id SERIAL PRIMARY KEY,
        message_text TEXT,
        user_id BIGINT NOT NULL,
        message_timestamp TIMESTAMP WITH TIME ZONE NOT NULL
    );
    """

    execute_query(create_table_query)
